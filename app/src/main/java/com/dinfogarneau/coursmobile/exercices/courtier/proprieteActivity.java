package com.dinfogarneau.coursmobile.exercices.courtier;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class proprieteActivity extends AppCompatActivity {

    private SharedPreferences prefs;
    private static final String SHARED_PREFERENCES = "shared_preferences";
    private static final int REQUEST_CODE = 1;

    private static final String NOM = "nom";
    private static final String GEO = "geo:";
    private static final String TEL = "tel:";
    private static final String PROPRIETE = "propriete";

    private static final String NUM_TEL_1 = "418-833-9785";
    private static final String NUM_TEL_2 = "418-957-0243";
    private static final String NUM_TEL_3 = "418-833-8765";

    private static final String LOCATION_1 = "46.7833699,-71.1917235";
    private static final String LOCATION_2 = "43.6425701,-79.3892455";
    private static final String LOCATION_3 = "45.4960704,-73.571504";

    private static final String SITE_1 = "https://garneau.com";
    private static final String SITE_2 = "https://www.cntower.ca/intro.html";
    private static final String SITE_3 = "https://www.centrebell.ca/en";

    private TextView tvNom;
    private ImageView ivParametres;
    private ImageView ivPropriete;
    private ImageView ivAppel;
    private ImageView ivLocation;
    private ImageView ivSite;
    private ImageView ivHappy;
    private ImageView ivNeutral;
    private ImageView ivSad;
    private ImageView ivBack;
    private ImageView ivNext;

    public ArrayList<Propriete> proprietes = new ArrayList<Propriete>();
    public int currentIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_propriete);

        prefs = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        tvNom = findViewById(R.id.tv_nom);
        tvNom.setText(prefs.getString(NOM, "bot"));

        ivParametres = findViewById(R.id.iv_parametres);
        ivPropriete = findViewById(R.id.iv_propriete);
        ivAppel = findViewById(R.id.iv_appel);
        ivLocation = findViewById(R.id.iv_location);
        ivSite = findViewById(R.id.iv_site);
        ivHappy = findViewById(R.id.iv_happy);
        ivNeutral = findViewById(R.id.iv_neutral);
        ivSad = findViewById(R.id.iv_sad);
        ivBack = findViewById(R.id.iv_back);
        ivNext = findViewById(R.id.iv_next);

        proprietes.add(new Propriete(R.drawable.maison, NUM_TEL_1, LOCATION_1, SITE_1, ReactionEnum.UNDEFINED));
        proprietes.add(new Propriete(R.drawable.maison4, NUM_TEL_2, LOCATION_2, SITE_2, ReactionEnum.UNDEFINED));
        proprietes.add(new Propriete(R.drawable.maison5, NUM_TEL_3, LOCATION_3, SITE_3, ReactionEnum.UNDEFINED));

        for (int i = 0; i < proprietes.size(); i ++) {
            int index = prefs.getInt(PROPRIETE + i, 0);
            ReactionEnum reaction = ReactionEnum.values()[index];
            if (i == 0) {
                this.setPropertyReaction(reaction);
            }
            proprietes.get(i).setReaction(reaction);
        }

        ivParametres.setOnClickListener(view -> {
            Intent intent = new Intent(proprieteActivity.this, parametresActivity.class);
            intent.putExtra(NOM, tvNom.getText().toString());
            startActivityForResult(intent, REQUEST_CODE);
        });

        ivNext.setOnClickListener(view -> {
            if (this.currentIndex == 2) {
                this.currentIndex = 0;
            } else {
                this.currentIndex += 1;
            }
            Propriete propriete = proprietes.get(this.currentIndex);
            ivPropriete.setImageResource(propriete.getImage());

            this.setPropertyReaction(propriete.getReaction());
        });

        ivBack.setOnClickListener(view -> {
            if (this.currentIndex == 0) {
                this.currentIndex = 2;
            } else {
                this.currentIndex -= 1;
            }
            Propriete propriete = proprietes.get(this.currentIndex);
            ivPropriete.setImageResource(propriete.getImage());

            this.setPropertyReaction(propriete.getReaction());
        });

        ivHappy.setOnClickListener(view -> {
            this.proprietes.get(this.currentIndex).setReaction(ReactionEnum.HAPPY);
            this.editIntSharedPreferences(PROPRIETE + this.currentIndex, ReactionEnum.HAPPY.ordinal());
            this.updateHappyProperty();
        });

        ivNeutral.setOnClickListener(view -> {
            this.proprietes.get(this.currentIndex).setReaction(ReactionEnum.NEUTRAL);
            this.editIntSharedPreferences(PROPRIETE + this.currentIndex, ReactionEnum.NEUTRAL.ordinal());
            this.updateNeutralProperty();
        });

        ivSad.setOnClickListener(view -> {
            this.proprietes.get(this.currentIndex).setReaction(ReactionEnum.SAD);
            this.editIntSharedPreferences(PROPRIETE + this.currentIndex, ReactionEnum.SAD.ordinal());
            this.updateSadProperty();
        });

        ivSite.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.proprietes.get(this.currentIndex).getWebsite()));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        });

        ivAppel.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(TEL + this.proprietes.get(this.currentIndex).getTelephone()));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        });

        ivLocation.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(GEO + this.proprietes.get(this.currentIndex).getLocation()));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        });
    }

    protected void onActivityResult(int req, int res, Intent data) {
        super.onActivityResult(req, res, data);
        if (req == REQUEST_CODE && res == RESULT_OK) {
            String nom = data.getStringExtra(parametresActivity.NOM);
            tvNom.setText(nom);
        }
    }


    private void editIntSharedPreferences(String key, int value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    private void setPropertyReaction(ReactionEnum reaction) {
        switch (reaction) {
            case HAPPY:
                this.updateHappyProperty();
                break;
            case NEUTRAL:
                this.updateNeutralProperty();
                break;
            case SAD:
                this.updateSadProperty();
                break;
            default:
                this.updateUndefinedProperty();
                break;
        }
    }

    private void updateHappyProperty() {
        ivHappy.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.happy)));
        ivNeutral.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
        ivSad.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
    }

    private void updateNeutralProperty() {
        ivNeutral.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.neutral)));
        ivHappy.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
        ivSad.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
    }

    private void updateSadProperty() {
        ivSad.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.sad)));
        ivNeutral.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
        ivHappy.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
    }


    private void updateUndefinedProperty() {
        ivSad.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
        ivNeutral.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
        ivHappy.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(NOM, tvNom.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        tvNom.setText(savedInstanceState.getString(NOM));
        super.onRestoreInstanceState(savedInstanceState);
    }
}