package com.dinfogarneau.coursmobile.exercices.courtier;

public enum ReactionEnum {
    UNDEFINED,
    HAPPY,
    NEUTRAL,
    SAD
}
