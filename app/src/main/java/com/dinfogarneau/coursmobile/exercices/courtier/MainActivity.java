package com.dinfogarneau.coursmobile.exercices.courtier;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{

    private static final String MSG_ERREUR_CONN = "Le pseudo ou mot de passe est incorrect!";
    private static final String MDP = "123";
    private static final String PSEUDO = "bot";

    private EditText etPseudo;
    private EditText etPassword;
    private Button btConnexion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etPseudo = findViewById(R.id.et_pseudo);
        etPassword = findViewById(R.id.et_password);
        btConnexion = findViewById(R.id.bt_connexion);

        btConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etPassword.getText().toString().equals(MDP) && etPseudo.getText().toString().equals(PSEUDO)) {
                    Intent intent = new Intent(MainActivity.this, proprieteActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, MSG_ERREUR_CONN, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}