package com.dinfogarneau.coursmobile.exercices.courtier;

public class Propriete {

    private int image;
    private String telephone;
    private String location;
    private String website;
    private ReactionEnum reaction;

    public Propriete(int image, String telephone, String location, String website, ReactionEnum reaction) {
        this.image = image;
        this.telephone = telephone;
        this.location = location;
        this.website = website;
        this.reaction = reaction;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public ReactionEnum getReaction() {
        return reaction;
    }

    public void setReaction(ReactionEnum reaction) {
        this.reaction = reaction;
    }
}
