package com.dinfogarneau.coursmobile.exercices.courtier;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class parametresActivity extends AppCompatActivity {

    public static final String NOM = "nom";
    private static final String PROPRIETE = "propriete";
    private static final String SHARED_PREFERENCES = "shared_preferences";
    private static final String MSG_RESET_REACTIONS = "Réactions réinitialiser";
    private static final int DEFAULT_REACTION = 0;
    private SharedPreferences prefs;

    private Button btResetEmoticons;
    private Button btOk;
    private EditText etNomPrenom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametres);
        prefs = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);

        btResetEmoticons = findViewById(R.id.bt_reset);
        btOk = findViewById(R.id.bt_ok);
        etNomPrenom = findViewById(R.id.et_nom_prenom);

        Intent intent = getIntent();
        etNomPrenom.setText(intent.getStringExtra(NOM));

        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(NOM, etNomPrenom.getText().toString());
                editor.apply();

                String nom = etNomPrenom.getText().toString();
                Intent intent = new Intent();
                intent.putExtra(NOM, nom);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        btResetEmoticons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = prefs.edit();
                for (int i = 0; i <=2; i++) {
                    editor.putInt(PROPRIETE + i, DEFAULT_REACTION);
                    editor.apply();
                }
                Toast.makeText(parametresActivity.this, MSG_RESET_REACTIONS, Toast.LENGTH_SHORT).show();
            }
        });
    }
}